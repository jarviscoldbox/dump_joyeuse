#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:4ce058bee9d61d859eaf1d3dcaaddb3d1d8e9bed; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:134217728:80895f0bfb4581d606eb1fefb31fa8a813c96210 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:4ce058bee9d61d859eaf1d3dcaaddb3d1d8e9bed && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
